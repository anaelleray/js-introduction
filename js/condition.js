let age = 20;

let isBirthday = false;

let myName = "Anaelle";

let teacher = "Demel" + "Brognard";
let year = 2019;
//On vérifie si le contenu de la variable myName correspond strictement
//au contenu de la variable teacher
if (myName === teacher) {
    console.log("Teacher is You");
} else {
    console.log("Student is You");
}

//On vérifie si la variable isBirthday est true
// if(isBirthday === true) {
if (isBirthday) {
    age++; //équivaut à...
    // age += 1; // ...qui équivaut à...
    // age = age+1;
    //On concatène le contenu de la variable myName avec la chaîne de
    //caractère "Happy Birthday ", on peut le faire soit avec un +
    console.log("Happy Birthday " + myName);
    //Soit avec les back ticks et le ${variable}
    //console.log(`Happy Birthday ${myName}`);
}
//On vérifie si la valeur de la variable age est supérieure ou égale à 18
if (age >= 18) {
    console.log("Cheers !");

} else {
    console.log("Have some juice !");
}
if (teacher === "Brognard" && isBirthday) {
    console.log("Pierre brings the cake");
}

if (age >= 30 && age <= 39) {
    console.log("my best year !");
}
if (isBirthday = false || age < 17) {
    console.log("i won't vote this year");
}
if(age < 30) {
    let thirtiesYear = year + 30 - age;
    console.log("Big party for my thirties in "+thirtiesYear+"!!!");
    
}