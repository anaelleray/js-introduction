let fridge = ["sprite", "coca", "vodka", "wisky", "jager"];
//let fridge2 = new Array("coca","champagne","limonade","water","badoit");

console.log(fridge);

// je sélectionne chacune des li par leur id
// let boisson1Li1 = document.getElementById('boisson1');
// let boisson1Li2 = document.getElementById("boisson2");
// let boisson1Li3 = document.getElementById("boisson3");
// let boisson1Li4 = document.getElementById("boisson4");
// let boisson1Li5 = document.getElementById("boisson5");

//console.log(fridge[3]); //water

// document.write("<h1>TOTO</h1>");

// je prends ma première li et je veux écrire dedans la première boisson de mon fridge
// boisson1Li1.innerHTML = fridge[0];
// boisson1Li1.textContent = fridge[0];
// document.getElementById("boisson1").innerHTML = fridge[0];

for (let parcourirFridge = 0; parcourirFridge < fridge.length; parcourirFridge++) {
    // console.log(fridge[parcourirFridge]);
    // 0 :     console.log(fridge[0]); -> console.log("fanta");
    // 1 :     console.log(fridge[1]); -> console.log("champagne");

    // sélection du bon li en fonction de mon index parcourirFridge
    let boisson = document.getElementById('boisson' + (parcourirFridge + 1));
    boisson.textContent = fridge[parcourirFridge];
}

let collection = ["tesla", "mazda", "kia", "fiat"];

for (let garage = 0; garage < collection.length; garage++) {

    let model = document.getElementById("voiture" + (garage
        + 1));
    model.textContent = collection[garage];

}
console.log(collection)