// var firstNumber = 10; //X Manière historique, mais maintenant ya mieux
// firstNumber = 10; //X Manière risqué pasque déclare en globale
// const firstNumber = 10; //Déclare une constante, donc une variable non modifiable

//On déclare une nouvelle variable
let firstNumber = 10;
//On modifie la valeur de la variable qu'on vient de créer (assignation)
firstNumber = 11;
//On affiche la valeur de la variable
console.log(firstNumber);

let firstName = 'Anaelle';
console.log(firstName);

let age = 20;
console.log(age);

